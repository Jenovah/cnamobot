const Discord = require('discord.js')
const bot = new Discord.Client()
import Planning from './commands/Planning'
import Help from './commands/Help'

bot.on('ready', () => {
    bot.user.setAvatar('img/bot.jpg')
    .catch(console.error)
})

bot.on('message', (message) => {
    let embedResponse = new Discord.MessageEmbed()
    const strSent = message.content.toLowerCase().split(' ')
    const channel = message.channel.name.toLowerCase()
    const action = strSent[0].startsWith('!') ? strSent[0] : null
    switch (action) {
        case '!planning':
                const data = {
                    message: message,
                    action: action[0],
                    module: strSent.length == 3 ? strSent[1] : channel.split('-')[0],
                    region: strSent[strSent.length -1]
                }
                const planning = new Planning(data, embedResponse)
                planning.action()
                break
        case '!help': 
                const help = new Help(message, embedResponse)
                help.action()
                break
        case null:
                break;
        default:
                message.channel.send('Désolé, je ne connais pas cette commande !\r\nTu peux consulter l\'aide via la commande suivante: !help')
    }
})

bot.login('NjkyNDM3NTQzNDc5NjcyODg1.Xn941w.xDcGQHoilerqO3TXQ2rDEGXsh8M')
