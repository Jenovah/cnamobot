import Utils from '../Utils'
const moment = require('moment')
moment.locale('fr')
const availableModule = {
    paca: [
        'utc504',
        'utc505',
    ],
    bretagne: [
        'utc502',
        'sec102'
    ],
    grandest: [
        'utc502',
        'utc503',
        'utc504',
        'rsx101'
    ]
}

export default class Planning {

    data = null
    embedResponse = null

    constructor(data, embedResponse) {
        this.data = data
        this.embedResponse = embedResponse
            .setColor('#BC0057')
            .setTitle('Voici le planning de la semaine pour ' + this.data.module + ' !')
            .setURL('https://lecnam.net/')
            .setAuthor('CnamoBot', 'https://cs11.pikabu.ru/post_img/2019/12/12/5/1576133915183631868.jpg')
            .setDescription('**__Région ' + this.data.region + '__**')
            .setThumbnail('https://cs11.pikabu.ru/post_img/2019/12/12/5/1576133915183631868.jpg')
            .setImage()
            .setTimestamp()
            .setFooter('CnamoBot')
    }

    action() {
        //args = [0] => codeUTC, [1] => region
        if (availableModule[this.data.region] != undefined && availableModule[this.data.region].includes(this.data.module)) {
            const filePath = './csv/' + this.data.region + '.csv'
            Utils.parseCSV(filePath, 
                {
                    success: results => this.sendFormattedData(results),
                    error: () => this.data.message.reply('Je n\'ai pas cette info, désolé !')
                }
            )
        } else {
            this.data.message.reply('Désolé, je n\'ai aucune info dans ma base de données concernant ' + this.data.module + ' !')
        }
    }

    sendFormattedData = (results) => {
        let currendDate = moment(new Date(), "DD/MM/YYYY")
        let currendDateWeekAdded = moment(new Date(), "DD/MM/YYYY").add('1', 'week')
        let emptyMessage = true
        results['data'].forEach(element => {
            let subject = element['Objet'].toLowerCase()
            let dateDebut = moment(element['dateDebut'], 'DD/MM/YYYY')
            
            if (subject.includes(this.data.module) && (dateDebut >=  currendDate && dateDebut <= currendDateWeekAdded)) {
                this.embedResponse.addFields(
                    {name: '**Le ' + dateDebut.format('L') + ' à ' + element['heureDebut'] + '**', value: '**' + element['Objet'] + '**', inline: true},
                    {name: '**Organisateur: **', value: '**' + element['Organisateur'] + '**', inline: true},
                    {name: "*", value: "*", inline:true}
                )
                emptyMessage = false
            }
        })
        if (!emptyMessage) {
            this.data.message.channel.send(this.embedResponse)
        } else {
            this.data.message.reply('Hmmm... Désolé je n\'ai rien concernant ' + this.data.module + ' pour cette semaine ! Bonnes vacances ! =)')
        }
    }



}

