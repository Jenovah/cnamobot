export default class Help {

    embedResponse = null
    message = null

    constructor(message, embedResponse) {
        this.message = message
        this.embedResponse = embedResponse
            .setColor('#BC0057')
            .setTitle('Un peu d\'aide ! ')
            .setURL('https://lecnam.net/')
            .setAuthor('CnamoBot', 'https://cs11.pikabu.ru/post_img/2019/12/12/5/1576133915183631868.jpg')
            .setDescription('**__Voici les commandes disponibles:__**')
            .setThumbnail('https://cs11.pikabu.ru/post_img/2019/12/12/5/1576133915183631868.jpg')
            .setImage()
            .setTimestamp()
            .setFooter('CnamoBot')
            .addFields(
                { name: '**Le planning**', value: '**!planning <module> <region>** : Affiche le planning de la semaine du module et de la région précisée.\r\n'
                + '**!planning <region>** : Affiche le planning de la semaine de la région précisée et du module qui correspond au channel de discussion actuel.' },
                { name: '**Les plannings disponibles**:', value: '**PACA:**\r\nUTC504, UTC505\r\n**Bretagne:**\r\nUTC502, SEC102\r\n**Grand Est:**\r\nUTC502, UTC503,UTC504, RSX101'},
            )
    }

    action() {
        this.message.channel.send(this.embedResponse)
    }
}