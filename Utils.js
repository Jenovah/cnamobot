const Papa = require('papaparse')
const fs = require('fs');

export default class Utils {
    static parseCSV (filePath, callback) {
        let file = fs.createReadStream(filePath)
        Papa.parse(file, 
            {
                delimiter: ",",
                worker: true,
                header: true,
                skipEmptyLines: true,
                dynamicTyping: true,
                encoding: 'latin1',
                complete: (results) => {
                    callback.success(results)
                },
                error: () => {
                    callback.error()
                }
            })
    }
}